FROM alpine:3.18.0

# Instalar Kubeconform
RUN apk add --no-cache curl && \
    curl -LO https://github.com/yannh/kubeconform/releases/download/v0.6.1/kubeconform-linux-amd64.tar.gz && \
    tar xvf kubeconform-linux-amd64.tar.gz kubeconform && \
    chmod +x kubeconform && \
    mv kubeconform /usr/local/bin/

